//
//  NewFeatureTutorialView.h
//  Markafoni
//
//  Created by Omer Aktuna on 6/3/14.
//  Copyright (c) 2014 Markafoni. All rights reserved.
//

#import "PageControl.h"

@protocol NewFeatureTutorialViewDelegate <NSObject>

- (void) newFeatureTutorialFinished;

@end

@interface NewFeatureTutorialView : UIView<UIScrollViewDelegate> {
    UIScrollView *newFeatureTutorialScroll;
    NSArray *images_array;
    PageControl *pageControl;
    UIButton *skipButton;
}

@property (nonatomic, strong) id <NewFeatureTutorialViewDelegate> delegate;

- (void)showWithImages:(NSArray *)imagesArray;
+ (BOOL)needsDisplay;

@end
